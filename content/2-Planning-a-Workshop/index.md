# Planning a workshop

#### Building a good workshop starts long before the participants show up. This section will address some of the key elements of good planning.

---

Presentation tools, technology and supplies - From simple things like whiteboards and erasers to more complex things such as a reliable Internet connection and an overhead projector, the underpinning of a good workshop is functioning presentation tools. Something as small as whiteboard markers that don’t work can bring a workshop to a screeching halt. Make a written list of the things you’ll need. Budget and arrange for them.

Here is a suggested checklist of supplies:

* Whiteboard
* HDMI connector and other essential cables
* Back up modem
* Erasers
* Markers
* Paper roll and stands
* Overhead projector
* Wifi
* Adequate tables and chairs

Technology is an especially important element of preparation. Different countries and regions have very different access to technology like reliable Internet and overhead projectors . Plan accordingly. **DOWNLOAD SAMPLE CONTENT AND EXAMPLES PRIOR TO THE WORKSHOP** and always have a backup plan in the event that technology fails.

Participant’s mobile data plans can be an important consideration for StoryMaker workshops. You need to carefully think this through related to assignments. Some projects will budget to provide participants useable data plans. Encourage participants to take advantage of wi  at the host location.

---

## The Partner Organization

Depending on how a workshop is sponsored and arranged the trainer may not be involved in some of the initial organization and decision-making. Still, these decisions can impact the training and the trainer should be informed and contribute to the planning as needed.

Most workshops are arranged through a partner organization. Small World News frequently works through a local partner or a reliable network of contacts. Good local partners can help navigate regulation and registration of workshops with local officials, help identify solid participants and locate a suitable home for training.

It is essential to have a good local partner.

Look for partners with a track record of successful workshops. From the outset it is important that the partner organization share our goals for training. Be sure to have a contract that clearly states deadlines and goals for the partner organization’s responsibilities. The contract or agreement should be clear on **EXACTLY** what the local partner provides.

If you and your partner organization is not doing its part during the planning, this a warning sign for what you can expect during the training itself.

---

## Participant Selection
A key role of the partner organization is selecting participants. Some work- shops are organized to provide training to a single organization. Others to a particular group. Free Press Unlimited organizes training for a diverse types of participants including journalists, citizen journalists, bloggers and activists.

If you are teaching the workshop alone a good target number is 8 - 12. If you have a partner or an assistant, you might push the number to 16. For workshops of 20 or more there should be at least three trainers. Participants can be broken into groups for exercises and to ensure more individualized attention.

Ideally, participants will attend due to personal interest rather than requirement. Good partner organizations vet participants carefully, through a rigorous application process. Bad partners often select participants without a lot of thought or select participants that serve the partner organization’s interests.

If you have some control over the selection process it is always good to have a variety of participants with a variety of skills and experience. In a well-run workshop participants can learn a great deal from each other if there is an environment that supports engagement. However, if there is too much of a range in skill, more experienced participants can become bored and even disruptive. More on this in Section 7 - Trainer Magic.

---

## Participant Assessment
It is important to do an assessment of participants prior to a workshop. A simple survey or questionnaire can tell the trainer a great deal about the skill level and interests of potential participants. This will help you plan your workshop appropriately for the specific audience. This is especially important for hands-on workshops.

---

## Training Location

Another key element of workshop planning is the location. The workshop location must be functional, secure, and easy to get to-especially for trainers who have travelled to an unfamiliar city and are staying at a hotel. Wifi is an almost essential required service of a good training location. The office space of many NGOs includes a training center. You need to con rm that the center or other location will be open the required hours for the workshop. It might be helpful to make sure the location is open beyond the scheduled  finish time of the workshop in case it runs late. Air conditioning or fans need to be supplied if it is hot. Tea and coffee are a basic service and should be provided. The number of tables and chairs should also be confirmed.

---

## A Certificate

A template for a certificate of attendance is included as a resource in this outline. These can be very important to participants. The training organization needs to take it seriously too.

---

## Evaluations

A standard workshop evaluation form used by Small World News is included as resource in Section Nine. The evaluation should be distributed to participants, filled out and collected before the workshop ends.

---

## Travel, Meals and Lodging

For larger workshops the responsibility for travel, local transportation, meals and lodging may be beyond the trainer. But, it is in your interest to con rm these things are properly managed. If they are not it can be very disruptive to your workshop. Participants will hold you responsible.

---
