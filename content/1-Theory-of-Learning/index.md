# Theory of Learning

### Good workshops are well organized and run by knowledgeable trainers. Good workshops are relevant the to community from which the participants come and they address the real-world challenges to better journalism within those communities.

To make this happen, we believe a good training must be based on a specific theory of learning: **Targeted Practiced Learning**.

---

If you are a trainer you have likely been a participant in one or more workshops. Some media workshops are very good but many -- too many -- are not. They fail in ways that prevent them from effectively teaching the material they set out to teach. Even worse, it can undermine a community’s confidence in the power of StoryMaker and diminish interest in future programming.

Small World News wants to share our theory of learning with you. We believe it to be the most effective way to run a workshop in which the goal is not just to provide information but also to teach a skill and change attitudes about what journalism should be.

It is important that trainers embrace these concepts because the trainer is essential to their the success of participants.

---

How do people learn best? More importantly for the purposes of this workshop what is the best way to teach adult and young adult learners not just a new skill but new views or values about journalism?

Too often the structure used for a workshop is simply an extension of how we were taught in school. That’s not the best way to run a workshop. In school the model is the teacher talks and the student listens and takes notes. Occasionally there is a question. Success is de ned only by the student’s ability to memorize and repeat the material exactly as the teacher presented it. This is called ‘passive information transfer.’ It may be effective for getting people to memorize material but it doesn’t work to engage people to really think for themselves and work as journalists.

We expect something more. In a workshop environment designed to teach adult learners material must be presented in a more dynamic way. An effective model for learning in this environment requires that participants be engaged in their own learning and encouraged to think for themselves. This is accomplished through robust debate, hands-on exercises and directly addressing barriers to learning such as an environment that discourage debate. This is ‘targeted practice learning.’

---

Targeted practice learning is built on three key steps:

1. A concept is introduced and discussed with ample use of real-world examples.
2. Participants practice the concept during the workshop by applying it themselves through a carefully designed hands-on activity.
3. The lesson concept is reinforced during a round-up discussion in which participants are shown how to apply the concept to their work after the workshop.

As you will see in the workshop outline section, each step or segment of the workshop has discussion and/or an exercise built in -- targeted practice learning.

It is important to note that successful targeted practice learning requires a lot from the trainer.

The key to a good workshop is organization and an engaging trainer. It is essential that the workshop trainer be well prepared for this workshop. Trainers should be:

* Familiar with the content provided in this guide and the workshop subject matter.
* Comfortable with any technology on which the
workshop is based.
* Prepared with personal experience, examples, and exercises relevant to the training prior to the workshop.

---

**Note:** Trainers should be prepared to spend ten hours or more organizing material prior to a workshop. This time will include gathering examples of up-to-date, local news content and/or study of the outline content material presented below. It is essential that example content be both timely and relevant to the participants and their communities.

The trainer needs to be confident in his or her abilities. If you are insecure about what and how you will present or if the workshop is not organized and in your control, it will undermine your success. Read this guide. Prepare. Leave little room for error.

---
