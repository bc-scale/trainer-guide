# Participant Types

You will inevitably get a variety of different skill levels and levels of enthusiasm among your participants. Managing this variety is one the toughest challenges for trainers. There is also the issue of the difficult participant or group of participants. Difficult participants can quickly hijack and undermine a workshop. In almost every case difficult participants can be managed by the trainer through simply addressing the issue directly and constructively. Below are four common profiles for difficult participants and some suggestions for how to handle them.

---

## The Know-It-All

As with many difficult participants the know-it-all is frequently trying to overcompensate for insecurity by projecting superiority. The know-it-all often overwhelms discussions. The know-it-all is too harshly critical of other participants and sometimes of the workshop itself.

As a first step in dealing with the know-it-all who overwhelms discussion when he or she begins to speak during discussion the trainer can say that the participant’s opinion is respected but you’d like to hear from other people in the group. Another technique might be to place a piece of candy in front of each person during the discussion when they speak. A participant with a giant pile of candy will quickly get the message.

If neither of these techniques work. You can ask to speak to the participant privately during a break or after the workshop. If possible have a third person present, an assistant or workshop organizer. Respectfully explain to the difficult participant the specific problem. Ask if they understand why
it is a problem. You might explain that it is disruptive to the group or that you simply want to give other participants equal opportunity.

If this does not work, especially if the participant is overtly disrespectful of you and others, you may need to move to the next level. Many workshop participants are sent by an employer or by a partner organization. Make it clear to the participant that if his/her behavior does not change you will have to speak the organizer and may ask them to leave without the certificate.

It is very rare that you will have to ask a participant to leave the workshop, but it is better than letting them prevent others from learning.

---

## The Reluctant Participant

Some are reluctant to participate because they are intimidated by the material or just shy. Some participants are made to come to workshops by their employer and do not wish to be there. Sometimes older participants can be difficult because they are intimidated by the new tools and changing media landscape. They do not feel younger participants are sufficiently respectful of their experience.

No matter the reason you need to make reluctant participants feel comfortable and engaged. If you sense a participant is not engaged, talk to them about it during a break. It may be an easy problem to address. With some older participant or those having trouble with the tools, it may help to arrange a bit of extra one-on-one time to talk about technology. You may be able to pair the participant up with someone who is more experienced. Always showcase the work of reluctant participants who make real progress, especially if it is overcoming a barrier such as shyness or self-confidence. You might call on the older participant to share her experience during relevant discussion.

---

## The Boss

Sometimes workshops are organized for a single  organization. Often the boss can by an ally in a workshop. A boss can help you identify key issues to address and identify the skill level of participants. But sometimes the boss can be difficult. The boss may feel he needs to make his superiority felt. He may contradict you during the workshop. He may intimidate participants in such a way that they do not speak freely. He may unfairly criticize some participants that he is not happy with or otherwise insert his agenda into the workshop. The boss may also come to only some of the training days yet expect a certificate. Or the boss may pull employees out of the workshop at random for assignments.

You need to assert your authority with the difficult boss. Be respectful but make it clear that it is YOUR workshop. If it is going to be effective -- and if his/her employees are going to get certificates -- he/she needs to follow the rules. You may wish to have a third person present for this conversation. If the workshop has been organized through a third party you may wish to have a representative there.

---

## The Exceptional Participant

In an odd way, the exceptional participant can be a problem. The exceptional participant can be disengaged or even disruptive because he or she is so advanced. The work of the exceptional participant can be intimidating to others.

It is important to acknowledge the problem with the exceptional participant. You might ask her to help out with others almost as an assistant. You might also ask the exceptional participant to do a short presentation on her work. Finally, you might alter the assignment or requirements of the exceptional participant to allow them to experiment or try new things that they do not have not yet had the opportunity to learn.

---
