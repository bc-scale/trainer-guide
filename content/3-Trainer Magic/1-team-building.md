# Team-building Tips

### In most workshops it is advisable to include team building exercises in your schedule.

Each in-class exercise can require participants to engage with another participant in the process. Require that each participant find a different partner for each exercise.

Below are three ideas for some additional team building exercises, ranging from the simple to the more elaborate.

1. You may choose to do something as simple as ask each participant to stand in the middle of circle of the other participants as they give their introduction.
2. You may ask the group to create a uni ed story from a set of sequential pictures or drawing you provide. The pictures are handed out randomly. Each person has a picture and they must discuss with each other how to build the best story. For a large workshop you might break them into smaller groups to run the exercise.
3. You might consider beginning each morning with some physical exercises. Physical activities throughout the day can be a valuable tool for keeping participants engaged and focused as the day goes on.

If you want to try a team-building exercise make sure it is one that you are comfortable with. No matter what you do it should be done quickly and with energy.

While the group dynamic is important it is also important to get to know each participant. Everyone has different challenges and barriers to learn- ing. Figure out how to help each participant to be a better learner. Make sure that everyone gets a bit of one-on-one time and build what you learn into the feedback you provide to that participant.

---
