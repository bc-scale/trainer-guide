# Trainer Magic

A good trainer does far more than merely teach a skill. A good trainer has practical experience and real-world wisdom. A good trainer inspires and empowers. A really good trainer is more than just an instructor, a good trainer is a mentor.

If you wish to be a top-notch trainer, you have to aspire to be a mentor. You have to really understand the different personalities of the individual participants and the dynamic of the group as a whole. A mentor teaches people how to take that concept or skill and extrapolate from it, how to think for themselves and continue to improve long after the workshop is over.

Guided discussion is a key part of good workshop instruction and mento- ring. As you critique work or talk about new ideas, encourage participants to figure out the answers for themselves rather than simply tell them the right answer. Instead of telling a participant that a shot or audio clip doesn’t work, ask the participant or group what they think of the shot or clip. Do they think it works? What would make it work better? You are encouraging them to think for themselves and reason out a solution they can apply themselves later.

Another important element of trainer magic is the group dynamic you create in your workshop.

The introductions at the beginning of the workshop can be a first step. Experienced trainers will often begin workshops with team building exercises intended to breakdown participant boundaries and build an environment of trust.

---
