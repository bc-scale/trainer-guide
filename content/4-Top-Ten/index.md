# Top Ten Characteristics of a Good Workshop

---

## 1. Built on targeted practiced learning

As outlined in Section Two, good trainings are designed to introduce a specific skill, practice that skills and then reinforce how the skill applies to the participant’s work after the training is concluded.

---

## 2. Inspiring

The workshop is designed to teach people a specific skill or skills. That skill will mean very little if participants are not inspired to use the skills they've learned. Many participants face tough working conditions. You need to inspire them to continue to apply what they’ve learned after the training is over.

---

## 3. Structured for engagement

Good trainings integrate presentation, discussion, group exercises and hands-on assignments with review to keep the participant intellectually engaged. No single element of the training should last more than an hour without a break or change of activity. Good workshops have a distinct tempo - not too fast and not too slow. They are broken into distinct segments. There are sufficient break times for lunch, for prayer and for informal discussion.

---

## 4. Relevant to the working environment of participants

Material presented in good workshops have real-world applications for participants. They focus on stories that can really be done. They use tools to which participant have access. The real-world application must be constantly reinforced during the workshop.

---

## 5. Enthusiastic and Informed trainer

That’s what this guide is about. The trainer needs to be con dent in his or her abilities. If you are insecure about what and how you will present or if the workshop is not organized and in your control, it will undermine your success. Read this guide. Prepare. Leave little room for error.

---

## 6. Clear goals

You need to make the goals of the workshop clear at the beginning and restate them throughout. You need to manage expectations and not over promise or let the workshop go off track. Too often work- shops become platforms for people to talk endlessly about abstract ethical dilemmas, troubling regulatory issues and poor working conditions. These are important issues but do not let them consume the workshop. Stay focused on the goal of teaching people to use StoryMaker to tell great stories. There is time in the outline during the final day of the workshop to discuss other issues.

---

## 7. Participant involvement

It is up to the trainer to get people involved in discussions, especially people who are reluctant to contribute. Conversely, a good trainer doesn’t allow a single person or small group to over- whelm discussion. Even during a workshop structured well for engagement you need to constantly work on participant involvement. Involved participants keep the workshop energized and keeps it real.

---

## 8. Disciplined

Rules for the workshop must be laid out at the very beginning of workshop outline. It is important for you the trainer to maintain these standards throughout the workshop. You will undoubtedly be challenged. Someone will show up late. Someone will miss days. Be  rm. Participants who do not respect the rules should not get a certificate. If participants have a personal or work-related crisis you may make an exception. You must respect the rules yourself. Nothing is worse than a trainer who arrives late or is clearly unprepared.

---

## 9. Good working environment

For a workshop to succeed participants must feel comfortable and safe. This is can be as simple as a comfortable chair and a fan or as complex as fast wi  and apps that work properly on different generations of Android. Participants also must feel they can ask questions and even question your authority. They need to feel protected from reprisals in the group, especially if a manager or editor is present.

---

## 10. Flexible when flexibility is required

This trainer’s guide provides a roadmap for how to plan and run a strong workshop. No trainer’s guide can anticipate the unexpected. This might be a loss of electricity or an important breaking news event during your training, or any one of a dozen unforeseen issues. You need to adapt. You need to be proactive. Do not be afraid to restructure the workshop in ways both large and small if required.

---
