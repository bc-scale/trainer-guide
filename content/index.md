# Become a Better Trainer

![](images/cover_400.jpg)

Welcome to Small World News' guide to being a better trainer. This guide is designed to provide trainers with a comprehensive roadmap to improve their  workshop skills. The guide includes a list of basic needs for every workshop, tips for effective workshop management, and some of SWN's key learning from more than ten years experience running workshops all over the world.

---
